<?php


use Phinx\Migration\AbstractMigration;

class CreateSbuTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $sbu = $this->table('sbu')->addTimestampsWithTimezone();
                $sbu->addColumn('title', 'string', ['limit' => 20])
                    ->addColumn('acronym', 'string', ['limit' => 10])
                    ->addColumn('created_by','integer', ['null' => true])
                    ->addColumn('updated_by','integer', ['null' => true])
                    ->addColumn('status', 'integer', ['null' => true])
                    ->save();
                    
                $sbu->addForeignKey('created_by', 'users', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                    ->addForeignKey('updated_by', 'users', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                    ->save();
    }
}
