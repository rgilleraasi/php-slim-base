<?php


use Phinx\Migration\AbstractMigration;

class CreateUserSbuTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $userSBU = $this->table('user_sbu',['id' => false]);
        $userSBU->addColumn('user_id', 'integer', ['null' => true])
                ->addColumn('sbu_id', 'integer', ['null' => true])
                ->save();

        $userSBU->addForeignKey('user_id', 'users', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                ->addForeignKey('sbu_id', 'sbu', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                ->save();        
    }
    
}
