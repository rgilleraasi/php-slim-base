<?php


use Phinx\Migration\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $users = $this->table('users');
        $users->addColumn('first_name', 'string', ['limit' => 20])
              ->addColumn('middle_name', 'string', ['limit' => 40, 'null' => true])
              ->addColumn('last_name', 'string', ['limit' => 40])
              ->addColumn('password', 'string', ['limit' => 100])
              ->addColumn('email', 'string', ['limit' => 30, 'null' => false])
              ->addColumn('position', 'string', ['limit' => 30])
              ->addColumn('status', 'string', ['limit' => 30])
              ->addColumn('created_at', 'datetime')
              ->addColumn('updated_at', 'datetime', ['null' => true])
              ->save();
    }
}
