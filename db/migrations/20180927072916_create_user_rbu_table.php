<?php


use Phinx\Migration\AbstractMigration;

class CreateUserRbuTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $userRBU = $this->table('user_rbu',['id' => false]);
        $userRBU->addColumn('user_id', 'integer', ['null' => true])
                ->addColumn('rbu_id', 'integer', ['null' => true])
                ->save();

        $userRBU->addForeignKey('user_id', 'users', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                ->addForeignKey('rbu_id', 'rbu', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                ->save();  
    }
  
}
