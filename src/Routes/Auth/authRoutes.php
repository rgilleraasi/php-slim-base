<?php

use Franchising\Controller\Auth\CAuthController;

$app->group('/auth', function () use ($app) {
    $app->post('/login', CAuthController::class . ':login');
    $app->post('/register', CAuthController::class . ':register');
});