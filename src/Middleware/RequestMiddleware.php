<?php

use Franchising\Helper\Exceptions\CInvalidApiKeyException;

class RequestMiddleware
{
    /**
     * Container variable
     *
     */
    private $container;

    /**
     * Create a new ResponseMiddleware instance.
     *
     */
    public function __construct($container) {
        $this->container = $container;
    }

    /**
     * Middleware that forms the response
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {   
        if (!$this->isValidAPIKey($request)) {
            // return new CInvalidApiKeyException();
        }

        $response = $next($request, $response);

        return $response;
    }

     /**
     * Check if the api key is valid
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     *
     * @return boolean
     */
    public function isValidAPIKey($request)
    {
        return $request->getHeaderLine('X-Api-Key') == getenv('API_KEY') ? true: false;
    }
}