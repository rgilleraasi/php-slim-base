<?php

require __DIR__ . '/RequestMiddleware.php';
require __DIR__ . '/ResponseMiddleware.php';

$container = $app->getContainer();

// CSRF Protection
// $app->add(new \Slim\Csrf\Guard);

// Authorization
$app->add(new Tuupola\Middleware\JwtAuthentication([
    "path" => ["/api/". env('APP_VER') ."/"],
    "ignore" => ["/api/". env('APP_VER') ."/auth"],
    "secret" => env('JWT_SECRET')
]));

// Request middleware
$app->add(new RequestMiddleware($container));

// Response middleware
$app->add(new ResponseMiddleware($container));