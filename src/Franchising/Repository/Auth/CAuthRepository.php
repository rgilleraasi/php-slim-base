<?php

namespace Franchising\Repository\Auth;

use Franchising\Model\CUser;
use Illuminate\Support\Facades\DB;

class CAuthRepository
{
    /**
     * Create a new CAuthRepository instance.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Get the user by email
     *
     * @param string $email
     *
     * @return boolean|\Franchising\Models\CUser
     */
    public function getUserByEmail($email)
    {
        return CUser::where('email', $email)->first();
    }

    /**
     * Register a new user
     *
     * @param \Franchising\Models\CUser $user
     *
     * @return boolean
     */
    public function registerUser($user)
    {
        // DB::transaction(function () {
            $userRegistration = new CUser;
            $userRegistration->first_name = $user->first_name;
            $userRegistration->last_name = $user->last_name;
            $userRegistration->email = $user->email;
            $userRegistration->password = $user->password;
            $userRegistration->position = 'Franchisee';
            $userRegistration->status = 1;
            $userRegistration->save();
        // });
    }
}
