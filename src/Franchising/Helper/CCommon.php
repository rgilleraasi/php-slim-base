<?php
namespace Franchising\Helper;

use Slim\Http\Response;
use Franchising\Helper\CMailer;
use Franchising\Model\CModelInterface;

class CCommon
{
     /**
     * Convert the request to associated model
     *
     * @param \Slim\Http\Request $request
     * 
     * @return \Franchising\Model\CModelInterface
     */
    public static function convertToModel($request, CModelInterface $model)
    {
        return $model->convert($request);
    }

     /**
     * Return array with error messages
     *
     * @param string $messages
     * 
     * @return array
     */
    public static function returnWithErrors($messages)
    {
        return [
            'success' => false,
            'errors' => is_array($messages) ? $messages : [$messages] 
        ];
    }

    /**
     * Return response with error messages
     *
     * @param \Slim\Http\Response $response
     * @param array $messages
     * 
     * @return response
     */
    public static function returnWithValidationErrors(Response $response, $messages)
    {
        return $response->withJson([
            'success' => false,
            'errors' => is_array($messages) ? $messages : [$messages] 
        ]);
    }
}
