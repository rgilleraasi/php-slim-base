<?php

namespace Franchising\Helper\Messages;

class CAuthMessages 
{
    const ACCOUNT_INVALID = 'Email or password is incorrect';
    const SUCCESS_REGISTER = 'Successfully Registered';
}