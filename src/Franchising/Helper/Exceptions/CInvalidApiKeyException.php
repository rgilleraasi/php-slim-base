<?php

namespace Franchising\Helper\Exceptions;

class CInvalidApiKeyException {
   
   public function __invoke($request, $response, $exception) {
        return $response
            ->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write('Invalid API Key');
   }
}