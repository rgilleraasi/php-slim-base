<?php

namespace Franchising\Service;

use Franchising\Repository\Auth\CAuthRepository;

class CUserDomain
{
    /**
     * CUser repository
     *
     * @var \Franchising\Repository\Auth\CAuthRepository
     */
    private $userRepository;

    /**
     * Create a new CUserDomain instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userRepository = new CAuthRepository();
    }

    /**
     * Get user by email
     *
     * @param string $email
     * 
     * @return array
     */
    public function getUserByEmail($email)
    {
       return $this->userRepository->getUserByEmail($email);
    }

    /**
     * Register a new user
     *
     * @param \Franchising\Model\CUser $user
     * 
     * @return array
     */
    public function registerUser($user)
    {
        return $this->userRepository->registerUser($user);
    }

    public function addUser()
    {
        // TODO
    }

    public function updateUser()
    {
        // TODO
    }

    public function archiveUser()
    {
        // TODO
    }

    public function reactivateUser()
    {
        // TODO
    }

    public function checkEmailIfExist() 
    {
        // TODO
    }
}