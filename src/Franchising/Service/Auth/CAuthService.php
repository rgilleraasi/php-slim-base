<?php

namespace Franchising\Service\Auth;

use DateTime;
use Franchising\Model\CUser;
use Franchising\Helper\CCommon;
use Franchising\Service\CUserDomain;
use Franchising\Helper\Messages\CAuthMessages;

class CAuthService
{
    /**
     * CUser business logic
     *
     * @var \Franchising\Service\CUserDomain
     */
    private $cUserDomain;

    /**
     * Create a new CAuthService instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->cUserDomain = new CUserDomain();
    }

    /**
     * Logged in the user
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * 
     * @return array
     */
    public function login($model)
    {
        $user = $this->cUserDomain->getUserByEmail(
            $model->getAttributes()['email']
        );

        if (!$user) {
            return CCommon::returnWithErrors(CAuthMessages::ACCOUNT_INVALID);
        }

        if (!password_verify($model->getAttributes()['password'], $user->password)) {
            return CCommon::returnWithErrors(CAuthMessages::ACCOUNT_INVALID);
        }

        $user->token = $this->generateToken($user);
        
        return [
            'success' => true,
            'data' => [
                'user' => [
                    'token' => $user->token,    
                    'firstName' => $user->first_name,    
                    'middleName' => $user->middle_name,    
                    'lastName' => $user->last_name,    
                    'email' => $user->email,    
                    'position' => $user->position,    
                    'status' => $user->status,    
                ]
            ]
        ];
    }

    /**
     * Register a new user
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * 
     * @return array
     */
    public function register($model)
    {

        $model['password'] = $this->convertPassword($model['password']);
        $user = $this->cUserDomain->registerUser(
            $model
        );

        return [
            'success' => true,
            'message' => [  
                CAuthMessages::SUCCESS_REGISTER
            ]
        ];
    }

    public function convertPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * Generate a new JWT token
     *
     * @param \Franchising\Models\CUser $user
     *
     * @return string
     */
    public function generateToken(CUser $user)
    {
        $appUrl = getenv('APP_URL');
        $appSecret = getenv('JWT_SECRET');

        $now = new DateTime();
        $future = new DateTime("now +2 hours");

        $payload = [
            "iat" => $now->getTimeStamp(),
            "exp" => $future->getTimeStamp(),
            "jti" => base64_encode(random_bytes(16)),
            'iss' => $appUrl,  // Issuer
            "sub" => $user->email,
        ];

        $token = \Firebase\JWT\JWT::encode($payload, $appSecret, "HS256");
        return $token;
    }
}
