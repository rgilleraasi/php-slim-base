<?php

namespace Franchising\Model;

use Illuminate\Database\Eloquent\Model;

class CUser extends Model implements CModelInterface
{
    /**
     * Users table
     */
    protected $table = 'users';
    /**
     * User columns
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'password',
        'role',
        'status',
    ];
    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        // return $this->belongsToMany('App\Model\CRoles');
        return $this->hasMany('App\Model\CRoles', 'created_by');
    }
    /**
     * Convert the request to associated model
     *
     * @param Request $request
     * @return $this
     */
    public function convert($request)
    {
        if ($request->getParam('id')) {
            $this->id = $request->getParam('id');
        }
        if ($request->getParam('firstName')) {
            $this->first_name = $request->getParam('firstName');
        }
        if ($request->getParam('lastName')) {
            $this->last_name = $request->getParam('lastName');
        }
        if ($request->getParam('email')) {
            $this->email = $request->getParam('email');
        }
        if ($request->getParam('password')) {
            $this->password = $request->getParam('password');
        }
        if ($request->getParam('role')) {
            $this->role = $request->getParam('role');
        }
        if ($request->getParam('status')) {
            $this->status = $request->getParam('status');
        }

        return $this;
    }
}