<?php

namespace Franchising\Handler\Auth;

use Respect\Validation\Validator as v;
use Franchising\Helper\CValidator;

class CAuthHandler
{
    /**
     * Respect Validator
     *
     * @var \Franchising\Helper\CValidator
     */
    private $validator;

     /**
     * Create a new CAuthHandler instance.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->validator = new CValidator();
    }

     /**
     * Validate login request
     * 
     * @param \Franchising\model\CUser $model
     * 
     * @return array
     */
    public function loginHandler($model)
    {
        $values = [
            'email' => $model['email'],
            'password' => $model['password']
        ];

        return $this->validator->validate($values,
            [
                'email'    => v::noWhitespace()->notEmpty(),
                'password' => v::noWhitespace()->notEmpty(),
            ]
        );
    }

    /**
     * Validate register request
     * 
     * @param \Franchising\model\CUser $model
     * 
     * @return array
     */
    public function registerHandler($model)
    {
        $values = [
            'firstName' => $model['first_name'],
            'lastName' => $model['last_name'],
            'email' => $model['email'],
            'password' => $model['password']
        ];

        return $this->validator->validate($values,
            [
                'firstName'    => v::noWhitespace()->notEmpty(),
                'lastName' => v::noWhitespace()->notEmpty(),
                'email' => v::noWhitespace()->notEmpty(),
                'password' => v::noWhitespace()->notEmpty()
            ]
        );
    }
}
