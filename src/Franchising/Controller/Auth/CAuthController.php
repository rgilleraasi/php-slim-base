<?php

namespace Franchising\Controller\Auth;

use Slim\Http\Request;
use Slim\Http\Response;
use Franchising\Model\CUser;

use Franchising\Helper\CCommon;
use Franchising\Helper\CValidationResult;
use Interop\Container\ContainerInterface;
use Franchising\Controller\BaseController;
use Franchising\Handler\Auth\CAuthHandler;
use Franchising\Service\Auth\CAuthService;
use Franchising\Helper\Messages\CAuthMessages;

class CAuthController extends BaseController
{
    /**
     * Users model
     *
     * @var \Franchising\model\CUser
     */
    private $userModel;

    /**
     * Service for this module
     *
     * @var \Franchising\Service\Auth
     */
    private $authService;

    /**
     * Validation for this module
     *
     * @var \Franchising\Handler\Auth
     */
    private $authHandler;

     /**
     * Create a new CAuthService instance.
     *
     * @param \Interop\Container\ContainerInterface
     * 
     * @return void
     */
    public function __construct(ContainerInterface $container)
    {
        $this->authService = new CAuthService();
        $this->userModel = new CUser();
        $this->authHandler = new CAuthHandler();
    }

    /**
     * Authenticate user
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     *
     * @return \Slim\Http\Response
     */
    public function login(Request $request, Response $response)
    {
        $model = CCommon::convertToModel($request, $this->userModel);
        $validation = $this->authHandler->loginHandler($model);
        
        if (!$validation->isValid()) {
            return CCommon::returnWithValidationErrors(
                    $response,
                    $validation->getErrors()
                );
        }
        $data = $this->authService->login($model);

        return $response->withJson($data);
    }

    /**
     * Register a new user
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     *
     * @return \Slim\Http\Response
     */
    public function register(Request $request, Response $response)
    {
        $model = CCommon::convertToModel($request, $this->userModel);
        $validation = $this->authHandler->registerHandler($model);

        if (!$validation->isValid()) {
            return CCommon::returnWithValidationErrors(
                    $response, 
                    $validation->getErrors()
                );
        }

        $data = $this->authService->register($model);

        return $response->withJson($data);
    }

    /**
     * Reset users password
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     *
     * @return \Slim\Http\Response
     */
    public function resetPassword(Request $request, Response $response)
    {
        // TODO reset password
    }

    /**
     * Change users password
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     *
     * @return \Slim\Http\Response
     */
    public function changePassword(Request $request, Response $response)
    {
        // TODO change password
    }
}